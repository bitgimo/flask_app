# Flask App

This is a CRUD (Create Read Update and Delete) app on articles and associating them with comments.

To get started, install ruby version 2.5.5.

Run the command in the command prompt gem install rails.

Select a directory where to create a new project.

Run rails new <your own project>.

Change directory to the one of the project in the command prompt by doing cd <your project>.

To create model run:

	rails generate model Article title:string text:text

The database used is on postgresql.

To install postgresql plugin:
	
	gem install pg
	
Then install bundle:

	bundle install
	
Replace sqlite3 in the Gemfile right in the app folder with pg.

Make sure the database is already created.

Run:
	
	rails db:migrate
		
We can create a new model to associate with Article.
	
	rails generate Comment body:string text:text refernces:Article

After that migrate the database.

From here on you can procede with the following guide: https://guides.rubyonrails.org/getting_started.html